import * as React from "react"
import NewlaunchCard, { NewlaunchProps } from './index'

interface NewlaunchGroupProps {
	Newlaunch: NewlaunchProps[]
	// Newlaunch?: string,
	// featured_image?: string,
	// badge?: string,
	// slogan?: string,
	// description?: string,
	// url?: string,
	
}

const NewlaunchGroup = ({ Newlaunch }: NewlaunchGroupProps) => {
  return (
    <div>
			<div className="header-tabs">
				
			</div>
			<div id="newlaunch-listings" className="blog-wrapper block md:flex flex-wrap justify-between relative">
				{
					Newlaunch.map((Newlaunch, index) => (
							<NewlaunchCard 
								key={index} {...Newlaunch}
								featured_image = {Newlaunch.featured_image}
								slogan = {Newlaunch.slogan}
								description = {Newlaunch.description}
							/>
						)
					)
				}
				<style jsx>{`
					.blog-wrapper {
					}
					.blog-image_wrapper:before {
						content: "1";
						position: absolute;
						left: 0;
						right: 0;
						bottom: 0;
						top: 0;
						text-indent: -999999px;
						overflow: hidden;
						background: linear-gradient(0.26deg, #000000 29.56%, rgba(255, 255, 255, 0) 43.52%, rgba(255, 255, 255, 0) 92.54%);
						opacity: 0.3;
					}
					@media screen and (min-width: 768px) { 
						.blog-wrapper {
						}
					}
					@media screen and (min-width: 500px) { 
						.blog-wrapper {			

						}
					}
				`}</style>
			</div>	
		</div>
  )
}

export default NewlaunchGroup