import * as React from "react"
import BannerStatic, { BannerProps } from './index'
import SlickSlider from 'react-slick'
import styled from 'styled-components'

const SliderWrapper = styled.div`
position: relative;
max-height: 550px;

@media (max-width : 767px) {
	.slick-slider ul.slick-dots {
    bottom: 23px;
    z-index: 1;
    left: 0;
    right: 0;
    display: flex !important;
    justify-content: center;
    align-items: center;
		flex-wrap: nowrap;
		pointer-events: none;
		 & li {
			margin: 0;
			padding: 0;
			vertical-align: middle;
			width: auto;
			height: auto;
			padding: 0;
			box-sizing: unset;
			z-index: 1;
			position: relative;
			margin: 0 5px;
				&.slick-active button:before {
					background: #5f5f5f;
					width: 10px;
					height: 10px;
				}
				& button {
					width: auto;
					height: auto;
					margin: 0;
					padding: 0;
					display: block;
					position: relative;
					&:before {
						opacity: 0.6;
						width: 8px;
						padding: 0;
						content: " ";
						background: #c0c0c0;
						border-radius: 100%;
						height: 8px;
						position: relative;
						display: block;
					}
				}
		}
	}
}
@media (min-width : 768px) {
  & .slick-slider  button.slick-arrow {
		position: absolute;
    max-width: 40px;
    background: #FFF;
    width: 100%;
    left: auto;
		right: 25px;
    top: auto;
    bottom: 0;
    height: 40px;
    border: 1px solid #c0c0c0;
		border-radius: 100%;
		z-index: 1;
			&:before {
				content: url(https://api.omh.app/store/cms/media/singapore/newlaunch/slider-next.svg);
				font-size: 0;
				opacity: 1;
				color: transparent;
				position: absolute;
				left: 50%;
				top: 50%;
				transform: translate(-50%, -50%);
			}
			&.slick-prev {
				right: 79px;
				 &:before {
						content: url(https://api.omh.app/store/cms/media/singapore/newlaunch/slider-next-v1.svg);
				 }
			}
			&.slick-disabled {
				opacity: 0.6;
			}
	} 
}
`

interface BannerSliderProps {
	Banner: BannerProps[]
}


const BannerSlider = ({ Banner }: BannerSliderProps) => {

  return (
    <div className="banner-wrapper mx-auto relative overflow-hidden">
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
			<SliderWrapper>
				<SlickSlider
					slidesToScroll={1}
					slidesToShow={1}
					dots= {true}
					infinite={false}
				>
					{
						Banner.map((Banner, index) => {
							return (
								<BannerStatic 
									key={index} {...Banner}
									imageUrlDesktop = {Banner.imageUrlDesktop}
									imageUrlTablet = {Banner.imageUrlTablet}
									imageUrlMobile = {Banner.imageUrlMobile}
									bannerTitleDesktop = {Banner.bannerTitleDesktop}
									bannerSubTitleDesktop = {Banner.bannerSubTitleDesktop}
									bannerTitleMobile = {Banner.bannerTitleMobile}
									bannerSubTitleMobile = {Banner.bannerSubTitleMobile}
									bannerButton = {Banner.bannerButton}
									bannerURL = {Banner.bannerURL}
								/>
							)
						})
					}
				</SlickSlider>
			</SliderWrapper>
			<style jsx>{`
				.banner-wrapper {
					max-height: 550px;
					margin: 0 0 65px;
				}
				.banner-wrapper .slick-slider  button {
					position: absolute;
					max-width: 40px;
					background: red !important;
					width: 100%;
					left: 0;
					right: 0;
				}
      `}</style>
    </div>
  )
}

export default BannerSlider