const config = {
  GTM_CONFIG: {
    gtmId: 'GTM-TCPHJ4X',
    auth: 'yYthFGS5OlfDoYHliv4hiw',
    preview: 'env-6',
    dataLayerName: 'affordability_calculator_page'
  },
  MEDIA_PATH: 'https://api.omh.app/store/cms/media',
  NAV_LINK: {
    home: 'https://omh.sg?UTM_Source=calculators&UTM_Campaign=affordability_calculator',
    mortgage: 'https://omh.sg/services/mortgage?UTM_Source=calculators&UTM_Campaign=affordability_calculator'
  },
  SPECIALIST_LINK: 'https://omh.sg/services/mortgage?UTM_Source=calculators&UTM_Campaign=affordability_calculator'
}

export default config