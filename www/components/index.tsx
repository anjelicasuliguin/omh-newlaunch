import * as React from 'react'
import MainSection from './MainSection'
import BannerSlider from './Banner/group'
import DistrictGroup from './District/group'
import UspGroup from './USP/group'
import CTA from './CTA'
import NewlaunchGroup from './Newlaunch/group'
import BlogGroup from './Blog/group'

import '../styles/styles.css'
// import axios from 'axios'
// import NewLaunchesList from '../services/newlaunches'
import config from './config'
import {
  bannerConfig,
  districtConfig,
  uspConfig,
  blogConfig,
  newlaunchConfig
} from '../modules'

interface NewlaunchProps {
  media_path?: any | 'https://api.omh.app/store/cms/media',
}

const Newlaunch = ({
  media_path = config.MEDIA_PATH
}: NewlaunchProps) => {

  // const [ newluanchConfigApi, getNewLaunchesList ] = React.useState({
  //   slogan: '',
  //   description: '',
  //   featured_image: '',
  // })

  // React.useEffect(() => {
  //   async function fetchDetails() {
  
  //     NewLaunchesList
     
  //     .getNewLaunchesList()
  //     .then(response => {
  //       const { data } = response
  //       const { _data } = data
  //       console.log(data)
  //       _data && _data.newluanches && getNewLaunchesList(_data.newluanches)
        
  //     })
  //   } fetchDetails()
    
  
  // }, [])
  
  // const {
  //   slogan,
  //   description,
  //   featured_image,
  // } = newluanchConfigApi
  

  return (
    <div className='wrapper row'>
      <div>
        <BannerSlider {...bannerConfig}/>
        <MainSection 
          marginMobile = {'0 0 45px'}
          marginDesktop = {'0 0 68px'}
          title = {'New launches'}
          subtitle = {'Discover Singapore’s newly-launched properties'}
          url = {'#'}
          button = {'See more projects'}>
            <NewlaunchGroup {...newlaunchConfig} />
        </MainSection>
        <MainSection
          marginMobile = {'0 0 45px'}
          marginDesktop = {'0 0 89px'}
          title = {'Explore by districts'}
          subtitle = {'Discover Singapore’s newly-launched properties'}>
            <DistrictGroup {...districtConfig} />
        </MainSection>
        <MainSection 
          marginMobile = {'0 0 45px'}
          marginDesktop = {'0 0 79px'}
          titlePosition = {'center'}
          title = {'What’s included'}
          subtitle = {'Discover Singapore’s newly-launched properties'}>
          <UspGroup {...uspConfig} />
        </MainSection>
        <MainSection 
          background = {'#F7F6F7'}
          marginMobile = {'0 0 45px'}
          marginDesktop = {'0 0 73px'}>
          <CTA 
            image = {`${media_path}/singapore/newlaunch/open-doodles-reading-side-1.svg`}
            title = {'Get updates on latest properties in Singapore'}
            excerpt = {'Sign up and get daily updates'}
            url = {'#'}
            button = {'Sign up'}
          />    
        </MainSection>
        <MainSection 
          marginMobile = {'0 0 56px'}
          marginDesktop = {'0 0 95px'}
          title = {'Helpful reads'}
          subtitle = {'Blogs that will help you on your home buying journey'}
          url = {'#'}
          button = {'See more projects'}
        >
          <BlogGroup {...blogConfig} />
        </MainSection>
      </div>
      <style jsx>{`
        
      `}</style>
    </div>
  )
}

export default Newlaunch
