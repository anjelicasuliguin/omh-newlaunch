import * as React from "react"

export interface UspProps {
	image?: string,
	title?: string,
	excerpt?: string,
}


const UspCard = ({ image, title, excerpt, }: UspProps) => (
	<div className="usp-container justify-between flex md:block relative w-full mx-auto overflow-hidden">
		<div className="usp-image_wrapper">
			<img src={image} alt={title}/>
		</div>
		<div className="usp-content_wrapper text-left md:text-center">
			<h3>{title}</h3>
			<p>{excerpt}</p>
		</div>
		<style jsx>{`
				.usp-container {
					max-width: 366px;
					box-sizing: border-box;
					padding: 0 20px;
					margin: 0 0 39px;
				}
				.usp-container:last-child {
					margin: 0;
				}
				.usp-image_wrapper img {
					max-width: 41px;
					margin: 0 auto 16px;
				}
				.usp-content_wrapper h3{
					color: #1C1C1C;
					font-weight: 600;
					font-size: 16px;
					line-height: 24px;
					margin: 0 0 14px;
				}
				.usp-content_wrapper p{
					color: #5F5F5F;
					font-size: 14.5px;
					line-height: 22px;
				}
				@media screen and (min-width: 768px) { 
					.usp-container {
						margin: 0;
					}
					.usp-content_wrapper h3{
						font-size: 20px;
						line-height: 30px;
						margin: 0 0 16px;
					}
					.usp-content_wrapper p{
						font-size: 16px;
						line-height: 24px;
					}
				}
				@media screen and (min-width: 500px) { 
					
				}
			
      `}</style>
    </div>
)

export default UspCard                                                                                        