import React from 'react'
import dynamic from 'next/dynamic'

const BaseLayout = dynamic(import('./base'))
const StandaloneLayout = dynamic(import('./standalone'))
const DefaultLayout = dynamic(import('./default'))

function PageLayout(Component: any, type: string) {
  let Layout: any

  switch(type) {
    case 'base':
      Layout = BaseLayout
      break
    case 'standalone':
      Layout = StandaloneLayout
      break
    default:
      Layout = DefaultLayout
  }

  return class extends React.Component {
    render() {
      return (
        <Layout>
          <Component />
        </Layout>
      )
    }
  }
}

export default PageLayout