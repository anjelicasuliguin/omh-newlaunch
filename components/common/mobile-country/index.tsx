import React from 'react'

interface iProps {
  countryList: Object;
  country: string;
  setCountry: Function;
  hasError: any;
  value: string;
  onChange(event: React.FormEvent<HTMLInputElement>): void;
  onBlur(event: React.FormEvent<HTMLInputElement>): void;
}

const MobileCountry: React.FunctionComponent<iProps> = ({
  onChange,
  onBlur,
  countryList,
  country,
  setCountry,
  hasError,
  value
}) => {

  const [countryListActive, setCountryListActive] = React.useState(false)

  return (
    <div className="md-input-main">
      <div className={`md-input-box flex items-center ${hasError ? 'hasError' : ''}`}>
        <div
          className={`absolute z-10 ${!countryListActive ? 'hidden' : ''}`}
          data-option="country-list"
        >
          {Object.keys(countryList).map((item, index) => (
            <div
              className="flex items-center"
              key={`${index}-${item}`}
              onClick={() => [setCountry(item), setCountryListActive(false)]}
            >
              <img
                src={(countryList as any)[index].img}
                style={{
                  width: '25px',
                  height: '20px',
                  paddingRight: '5px'
                }} />
              +{(countryList as any)[index].phone}
            </div>
          ))}
        </div>

        {Object.keys(countryList).length !== 0 &&
          <div
            className="flex items-center justify-center"
            data-option="mobile-select"
            onClick={() => setCountryListActive(true)}
          >
            <div
              style={{
                width: '25px',
                height: '20px',
                paddingRight: '5px'
              }}
            >
              <img src={(countryList as any)[country].img} />
            </div>
            <div className="text-15">+{(countryList as any)[country].phone}</div>
          </div>}

        <input
          name="mobileNumber"
          pattern="^[0-9-+\s()]*$"
          type='tel'
          className="md-input font-thin"
          onChange={onChange}
          onBlur={onBlur}
          placeholder={' '}
          value={value}
        />
        <div className="md-label">Mobile Number</div>
      </div>


      <div
        className={`absolute w-screen h-screen bg-transparent top-0 bottom-0 left-0 right-0 ${!countryListActive ? 'hidden' : ''}`}
        onClick={() => setCountryListActive(false)}
      />
      <style jsx>{`
        [data-option="country-list"] {
          left: 0;
          background-color: #FFFFFF;
          overflow-x: hidden;
          overflow-y: auto;
          width: 100px;
          min-height: 300px;
          -webkit-overflow-scrolling: touch;
          box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0.14), 0px 3px 14px 2px rgba(0,0,0,0.12);
          height: 300px;
          top: -40px;
        }
        [data-option="country-list"] > div {
          width: auto;
          overflow: hidden;
          font-size: 1rem;
          box-sizing: border-box;
          min-height: 48px;
          font-family: "Roboto", "Helvetica", "Arial", sans-serif;
          font-weight: 400;
          line-height: 1.75;
          padding-top: 4px;
          white-space: nowrap;
          letter-spacing: 0.00938em;
          padding-bottom: 4px;
          padding-left: 16px;
          padding-right: 16px;
        }
        .md-input-box {
          position: relative;
          border-radius: 4px;
          border: 1px solid #DADCDF;
          box-sizing: border-box;
        }
        .md-input-box.hasError {
          border: 1px solid #f44336;
        }
        .md-input-box:active,
        .md-input-box:focus,
        .md-input-box:focus-within {
          border: 1px solid #72C7DB;
        }
        .md-input {
          width: 100%;
          outline: none;
          height: 48px;
          padding-left: 25px;
          font-size: 1rem;
        }
        [data-option="mobile-select"] {
          min-width: 80px;
          min-height: 48px;
          border-right: 1px solid #DADCDF;
        }
        .md-label {
          background-color: #FFFFFF;
          color: #9B9B9B;
          position: absolute;
          pointer-events: none;
          transform-origin: top left;
          transform: translate(100px, 0) scale(1);
          transition: color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms, transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms;
        }
        .md-input:focus + .md-label, .md-input:not(:placeholder-shown) + .md-label {
          color: #9B9B9B;
          transform: translate(100px, -20px) scale(0.75);
          transform-origin: top left;
          font-size: 1rem;
          padding: 5px;
        }
      `}</style>
    </div>
  )
}

export default MobileCountry
