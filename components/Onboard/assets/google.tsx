import React from 'react'

function IconGoogle() {
  return (
    <svg width="22" height="14" viewBox="0 0 22 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M7 6V8.4H10.97C10.81 9.43 9.77 11.42 7 11.42C4.61 11.42 2.66 9.44 2.66 7C2.66 4.56 4.61 2.58 7 2.58C8.36 2.58 9.27 3.16 9.79 3.66L11.69 1.83C10.47 0.69 8.89 0 7 0C3.13 0 0 3.13 0 7C0 10.87 3.13 14 7 14C11.04 14 13.72 11.16 13.72 7.16C13.72 6.7 13.67 6.35 13.61 6H7Z" fill="white" />
      <path fillRule="evenodd" clipRule="evenodd" d="M22 6H20V4H18V6H16V8H18V10H20V8H22" fill="white" />
    </svg>
  )
}

export default IconGoogle
