import React from 'react'

function IconClose() {
  return (
    <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
      <path opacity="0.5" fillRule="evenodd" clipRule="evenodd" d="M1.12538 0L0 1.12538L6.87462 8L0 14.8746L1.12538 16L8 9.12538L14.8746 16L16 14.8746L9.12538 8L16 1.12538L14.8746 0L8 6.87462L1.12538 0Z" fill="black"></path>
    </svg>
  )
}

export default IconClose
