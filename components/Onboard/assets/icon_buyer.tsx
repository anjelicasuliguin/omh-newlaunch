import * as React from "react"

function iconBuyer() {
  return (
    <div>
      <svg width="23" height="23" viewBox="0 0 23 23" fill="none">
        <path fillRule="evenodd" clipRule="evenodd" d="M0 8.05C0 3.61773 3.61773 0 8.05 0C12.4823 0 16.1 3.61773 16.1 8.05C16.1 10.0602 15.3502 11.8968 14.1234 13.3104L14.6131 13.8H16.1L23 20.7L20.7 23L13.8 16.1V14.6131L13.3104 14.1234C11.8968 15.3502 10.0602 16.1 8.05 16.1C3.61773 16.1 0 12.4823 0 8.05ZM13.8 8.05C13.8 4.86074 11.2393 2.3 8.05 2.3C4.86074 2.3 2.3 4.86074 2.3 8.05C2.3 11.2393 4.86074 13.8 8.05 13.8C11.2393 13.8 13.8 11.2393 13.8 8.05Z" fill="#EE620F" />
      </svg>
    </div>
  )
}

export default iconBuyer