import * as React from "react"

function Test() {
  return (
    <div />
  )
}

// function Counter(props) {

//   const {
//     count,
//     setCount,
//   } = props

//   function format(time) {
//     let seconds = time % 60;
//     let minutes = Math.floor(time / 60);
//     let testMin = minutes.toString().length === 1 ? "0" + minutes : minutes;
//     let testSeconds = seconds.toString().length === 1 ? "0" + seconds : seconds;
//     return testMin + ':' + testSeconds;
//   }

//   function useInterval(callback, delay) {
//     const savedCallback = React.useRef()

//     // Remember the latest function.
//     React.useEffect(() => {
//       savedCallback.current = callback
//     }, [callback])

//     // Set up the interval.
//     React.useEffect(() => {
//       function tick() {
//         savedCallback.current()
//       }

//       if(delay !== null) {
//         let id = setInterval(tick, delay)
//         return () => clearInterval(id)
//       }
//     }, [delay])
//   }

//   function runTime() {
//     if(count > 0) {
//       useInterval(() => {
//         setCount(count - 1)
//       }, 1000)
//     } else {
//       setCount(0)
//       return null
//     }
//   }

//   runTime()

//   return (
//     <div
//       data-counter="counter"
//     >
//       {format(count)}
//       <style jsx>{`
//         [data-counter="counter"] {
//           color: #EE620F;
//         }
//       `}</style>
//     </div>
//   )
// }

// export default Counter