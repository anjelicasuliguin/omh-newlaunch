import React from 'react'
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// service
import OnboardService from "../../../../services/api/onboard"

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'
import BtnSocial from '../../components/btn-social'
import FacebookLogin from './components/facebook'
import GoogleLogin from './components/google'

// assets
import Artwork from '../../assets/artwork'
import Facebook from '../../assets/facebook'
import Google from '../../assets/google'
import Close from '../../assets/close'

function SignupHome({ hooks }) {
  const {
    country,
    setCountry,
    countryList,
    setCountryList,
    setBearer,
    setUserData,
    drawerToggle,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
    loginState,
    setLoginState,
    step,
    setSocial,
    setTokensetSocial,
    setToken,
  } = hooks

  const [isFacebookActive, setIsFacebookActive] = React.useState(false)
  const [isGoogleActive, setIsGoogleActive] = React.useState(false)
  const [isRequesting, setIsRequesting] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  function loginFacebook(response) {
    setLoading(true)
    setIsRequesting(true)
    setUserData({
      email: response.email,
      firstName: response.first_name,
      lastName: response.last_name,
    })
    setIsFacebookActive(false)
    try {
      OnboardService
        .loginFacebook({ "token": response.accessToken })
        .then(response => {
          setSocial('facebook')
          setLoading(false)
          setIsRequesting(false)

          if(response && response.data._data !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
            document.body.style.overflow = 'auto'
          }

          if(response.data._data.user.altMobile !== undefined) {
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
            document.body.style.overflow = 'auto'
          } else {
            // setUserToken(response.headers["x-amzn-remapped-authorization"])
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            setLoginState(false)
            document.body.style.overflow = 'auto'
          }
        })
    } catch {
      console.error('error')
    }
  }

  function loginGoogle(response) {
    setUserData(response.profileObj)
    setLoading(true)
    setIsRequesting(true)
    try {
      OnboardService
        .loginGoogle({ "token": response.tokenId })
        .then(response => {
          setSocial('google')
          setLoading(false)
          setIsRequesting(false)

          if(response.data._data && response.data._data.token !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
            document.body.style.overflow = 'auto'
          }

          if(response.data._data.user.altMobile !== undefined) {
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
            document.body.style.overflow = 'auto'
          } else {
            // setUserToken(response.headers["x-amzn-remapped-authorization"])
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            setLoginState(false)
            document.body.style.overflow = 'auto'
          }
        })
    } catch {
      console.error('error')
    }
  }

  function renderView() {
    if(screen.current !== 'signup_home') {
      return null
    }

    return (
      <div
        data-login="login-home"
      >
        <div
          className="flex items-center justify-center"
        >
          <Artwork width="197" height="150" />
        </div>

        <div className="flex flex-col items-center text-center">
          <Heading
            title="Join Ohmyhome"
            subtitle="We're happy to see you, signup in below"
          />
        </div>

        <div
          className="w-full"
        >
          <div
            className="mb-20"
          >
            <FacebookLogin
              isFacebookActive={isFacebookActive}
              loginFacebook={loginFacebook}
              setIsFacebookActive={setIsFacebookActive}
            />
          </div>
          <div
            className="mb-20"
          >
            <GoogleLogin
              loginGoogle={loginGoogle}
            />
          </div>
          <div
            className="mb-20 w-full"
          >
            <Btn
              type="outlined"
              label="Sign up with Mobile"
              action={() => setView("signup_proper", screen.current)}
            />
          </div>
          <div
            className="text-14 text-center"
          >
            Already have an account? <span className="text-orange" onClick={() => setView("login_home")}>Log in</span>
          </div>
        </div>
        <style jsx>{`
          [data-login="login-home"] {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
            padding-top: 0px;
          }
        `}</style>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupHome

