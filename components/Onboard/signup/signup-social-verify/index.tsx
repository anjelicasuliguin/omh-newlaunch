import * as React from "react"
import { Formik } from 'formik'
import * as Yup from 'yup'
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'
import ErrorForm from '../../components/error-form'
import Textfield from '../../../common/textfield'
import MobileCountry from '../../../common/mobile-country'

// config
import OnboardService from '../../../../services/api/onboard'

// components
import { VerifyValidation } from './validation'

function SignupSocialVerify({ hooks }) {
  const {
    country,
    setCountry,
    token,
    setToken,
    social,
    countryList,
    userData,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
  } = hooks

  const [passwordValues, setPasswordValues] = React.useState({
    password: "",
    showPassword: false,
  })
  const [confirmPasswordValues, setConfirmPasswordValues] = React.useState({
    confirmPassword: "",
    showConfirmPassword: false,
  })
  const toggleDefaultPassword = () => {
    setPasswordValues({
      ...passwordValues,
      showPassword: !passwordValues.showPassword
    })
  }

  const toggleConfirmPassword = () => {
    setConfirmPasswordValues({
      ...confirmPasswordValues,
      showConfirmPassword: !confirmPasswordValues.showConfirmPassword
    })
  }

  const saveValue = (value) => {
    setFormData({ ...formData, mobile: value })
  }

  function handleSocial(type: string, params: Object) {
    if(type === 'facebook') {
      OnboardService
        .signupFacebook(params)
        .then((response) => {
          setToken(response.data._data.token)
          setView('signup_otp', screen.current)
        })
    } else {
      OnboardService
        .signupGoogle(params)
        .then((response) => {
          setToken(response.data._data.token)
          setView('signup_otp', screen.current)
        })
    }
  }

  function renderView() {
    if(screen.current !== "signup_socialverify") {
      return null
    }
    return (
      <div
        className="flex flex-col"
      >
        <Mobile>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Let's set you up"
              subtitle="Don't worry - this won't take long."
            />
          </div>
        </Desktop>

        <div>
          <Formik
            initialValues={{
              countryCode: "",
              mobileNumber: "",
              emailAddress: userData.email,
              token: token,
              social: social,
            }}
            onSubmit={(values, { setSubmitting }) => {
              const params = {
                "token": token,
                "countryCode": `${countryList[country].code}`,
                "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                "email": values.emailAddress
              }
              handleSocial(social, params)
            }}
            validationSchema={VerifyValidation(
              countryList[country] === undefined ||
                countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
          >
            {props => {
              const {
                values,
                touched,
                errors,
                dirty,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                handleReset,
              } = props
              return (
                <form onSubmit={handleSubmit}>
                  <div
                    className="mb-25"
                  >
                    <MobileCountry
                      onChange={handleChange}
                      onBlur={handleBlur}
                      countryList={countryList}
                      country={country}
                      setCountry={setCountry}
                      hasError={errors.mobileNumber && touched.mobileNumber}
                      value={values.mobileNumber}
                    />
                    {errors.mobileNumber && touched.mobileNumber && <ErrorForm>{errors.mobileNumber}</ErrorForm>}
                  </div>

                  <div
                    className="flex justify-between items-center mb-25"
                  >
                    <div
                      className="text-orange text-13 cursor-pointer"
                      onClick={() => setView(screen.previous)}
                    >
                      Back
                    </div>
                    <button
                      type="submit"
                      onClick={() => handleSubmit()}
                      className="h-btn bg-orange text-white text-15 px-25 rounded"
                    >
                      {isSubmitting ? "Submitting" : "Continue"}
                    </button>
                  </div>
                </form>
              )
            }}
          </Formik>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupSocialVerify