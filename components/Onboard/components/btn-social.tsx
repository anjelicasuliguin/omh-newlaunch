import React from 'react'

const config = {
  'facebook': {
    'background': '#507CC0'
  },
  'google': {
    'background': '#DC4E41'
  }
}

interface iProps {
  action: Function;
  label: string;
  icon: any;
  iconPosition: string;
  type: string;
}

const BtnGoogle: React.FunctionComponent<iProps> = ({
  action,
  label,
  icon,
  iconPosition,
  type,
}) => {
  return (
    <button
      className="btn-social"
      style={{
        backgroundColor: (config as any)[type].background
      }}
      onClick={() => action()}
    >
      {iconPosition === "left"
        ? (
          <React.Fragment>
            <span>{icon()}</span>
            <span className="btn-label">{label}</span>
          </React.Fragment>
        )
        : (
          <React.Fragment>
            <span>{label}</span>
            <span className="btn-label">{icon()}</span>
          </React.Fragment>
        )
      }
      <style jsx>{`
        .btn-social {
          display: flex;
          align-items: center;
          justify-content: center;
          min-height: 45px;
          width: 100%;
          border-radius: 4px;
          box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.0585118);
        }
        .btn-label {
          font-size: 15px;
          color: #FFFFFF;
          padding: 0 8px;
        }
      `}</style>
    </button>
  )
}

export default BtnGoogle
