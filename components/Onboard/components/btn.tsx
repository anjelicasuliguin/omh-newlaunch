import React from 'react'

interface IBtn {
  action: Function;
  label: string;
  type: string;
  icon?: any;
  iconPosition?: string;
}

const Btn: React.FunctionComponent<IBtn> = ({
  action = null,
  label,
  type,
  icon = null,
  iconPosition = null
}) => {

  const styleClass = type

  return (
    <button
      className={`btn btn-${type}`}
      onClick={() => action()}
    >
      {iconPosition === "left" && iconPosition !== undefined
        ? (
          <React.Fragment>
            {icon && icon()}
            <span className="btn-label">{label}</span>
          </React.Fragment>
        )
        : (
          <React.Fragment>
            <span className="btn-label">{label}</span>
            {icon && icon()}
          </React.Fragment>
        )
      }

      <style jsx>{`
        .btn {
          display: flex;
          align-items: center;
          justify-content: center;
          min-height: 45px;
          border-radius: 4px;
          width: inherit;
          padding: 5px 16px;
        }
        .btn-default {
          background-color: #EE620F;
        }
        .btn-default > .btn-label {
          color: #FFFFFF;
        }
        .btn-outlined {
          border: 1px solid rgba(0, 0, 0, 0.23);
        }
        .btn-outlined > .btn-label {
          color: #000000;
        }
        .btn-label {
          font-size: 15px;
          color: #FFFFFF;
          padding: 0 8px;
        }
      `}</style>
    </button>
  )
}

export default Btn
