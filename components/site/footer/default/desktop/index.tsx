import React from 'react'
import Link from 'next/link'

import Style from './style'

import facebook from '../../../../../assets/svg/facebook'
import instagram from '../../../../../assets/svg/instagram'
import youtube from '../../../../../assets/svg/youtube'

function FooterDefault() {
  return (
    <div
      className="container mx-auto md:px-25 py-25"
    >
      <div
        className="flex flex-row"
      >
        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-lg pb-5"
          >
            Follow us
          </h2>
          <div
            className="flex items-center"
          >
            <div className="pr-10">{facebook()}</div>
            <div className="pr-10">{instagram()}</div>
            <div className="pr-10">{youtube()}</div>
          </div>
        </div>

        <div
          className="flex-1 flex flex-col justify-center"
        >
          <div
            className="flex flex-col pb-6"
          >
            <span className="text-md text-darkGray pb-1">Singapore - Ohmyhome Pte Ltd</span>
            <span className="text-sm text-gray font-light">CEA License No. L3010739Z</span>
          </div>
          <div
            className="flex flex-col"
          >
            <span className="text-md text-orange pb-1">Need help?</span>
            <span className="text-sm text-gray font-light">Singapore :  +65 6886 9009</span>
          </div>
        </div>

      </div>
    </div>
  )
}

export default FooterDefault
