import React from 'react'
import Link from 'next/link'

import Style from './style'

import facebook from '../../../../../assets/svg/facebook'
import instagram from '../../../../../assets/svg/instagram'
import youtube from '../../../../../assets/svg/youtube'

function FooterDefault() {
  return (
    <div
      data-footer="container"
      className="py-6 border-b border-lighterGray mx-5"
    >
      <div>
        <ul
          className="flex"
        >
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none border-r	border-darkGray"
          >
            <Link
              href="#"
            >
              <a>Ohmyhome</a>
            </Link>
          </li>
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none border-r	border-darkGray"
          >
            <Link
              href="#"
            >
              <a>About us</a>
            </Link>
          </li>
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none"
          >
            <Link
              href="#"
            >
              <a>Useful links</a>
            </Link>
          </li>
        </ul>
      </div>

      <div
        className="flex flex-col items-center pt-6"
      >
        <span className="text-sm text-darkGray">Singapore - Ohmyhome Pte Ltd</span>
        <span className="text-xs text-gray font-light">CEA License No. L3010739Z</span>
      </div>

      <div
        className="flex flex-col items-center pt-6"
      >
        <span className="text-sm text-darkGray">Need help?</span>
        <span className="text-xs text-gray font-light">Singapore :  +65 6886 9009</span>
      </div>

      <div
        className="flex items-center justify-center pt-6"
      >
        <div className="px-2">{facebook()}</div>
        <div className="px-2">{instagram()}</div>
        <div className="px-2">{youtube()}</div>
      </div>
      {Style()}
    </div>
  )
}

export default FooterDefault
