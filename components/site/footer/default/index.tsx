import React from 'react'
import Responsive from 'react-responsive'

import MobileContent from './mobile'
import DesktopContent from './desktop'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

export default function FooterSeo() {
  return (
    <div
      className="md:py-10 lg:py-10 xl:py-10"
    >
      <Mobile>
        <MobileContent />
      </Mobile>

      <Tablet>
        <DesktopContent />
      </Tablet>

      <Desktop>
        <DesktopContent />
      </Desktop>
    </div>
  )
}
