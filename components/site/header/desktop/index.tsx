import React from 'react'
import Link from 'next/link'

// components
import CountryPicker from '../component/country-picker'
import Guest from '../component/guest'
import LoggedIn from '../component/logged-in'

import Call from '../../../../assets/svg/call'
import Menu from '../../../../assets/svg/menu-burger'
import Logo from '../../../../assets/svg/logo-full'


const orange = '#EE620F'
const white = '#FFFFFF'


const defaultLinks = [
  {
    text: 'Buy',
    url: '/buy'
  },
  {
    text: 'Sell',
    url: '/sell'
  },
  {
    text: 'Rent',
    url: '/rent'
  },
  {
    text: 'Housing solutions',
    url: '/housing-solutions'
  }
]

function Header({
  headerProps,
  handleLogout,
  standalone = false,
  links = null
}) {

  const {
    loginState,
    setLoginState,
    userData
  } = headerProps

  const isLoggedIn = userData && Object.keys(userData).length !== 0

  const headerLinks = links || defaultLinks

  return (
    <div
      className="flex px-35"
    >
      <div
        className="flex flex-1"
      >
        <div
          className="flex flex-1-auto items-center"
        >
          <div
            className=""
          >
            {
              standalone ? 
                <a href='https://omh.sg/'> {Logo(orange)} </a>
              : <a href='/'> {Logo(orange)} </a>
            }
          </div>
          <div className="flex h-headerMobile items-center">
            {
              headerLinks && headerLinks.map( item => {
                const { text, url } = item
                return(
                  <div
                    className="h-full"
                  >
                    <Link prefetch={false} href={url}>
                      <a className="link-item-container h-full relative block">
                        <span className="text-15 text-darkGray flex items-center px-headerNav h-full link-item">{text}</span>
                        <span className="border-item" />
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </div>
          <style jsx>{`
            .link-item-container {
              overflow: hidden;
              cursor: pointer;
            }

            .link-item-container:hover > .border-item {
              left: 0;
              opacity: 1;
            }

            .link-item-container:blur > .border-item {
              left: 100%;
              opacity: 1;
            }
            .border-item {
              display: block;
              height: 2px;
              width: 100%;
              position: absolute;
              bottom: 0;
              left: -100%;
              background-color: #EE620F;
              opacity: 0;
              transition: all 0.2s ease-in-out;
            }
          `}</style>
        </div>

        <div
          className="flex flex-1 items-center justify-end"
        >
          {
            standalone ? ''
            : 
            <React.Fragment>
              <div
                className="flex h-headerMobile items-center"
              >
                <button
                  className="bg-orange text-white h-headerBtn px-4 rounded text-14"
                >
                  List your place
                </button>
              </div>

              <div
                className="pl-20"
              >
                <CountryPicker />
              </div>

              {isLoggedIn
                ? (
                  <div
                    className="pl-20"
                  >
                    <LoggedIn
                      userData={userData}
                      handleLogout={handleLogout}
                    />
                  </div>
                ) : (
                  <div
                    className="pl-20"
                  >
                    <Guest
                      setLoginState={setLoginState}
                    />
                  </div>
                )
              }
            </React.Fragment>
          }
          <div
            className="flex h-headerMobile items-center pl-20"
          >
            <a href={'tel:65 6886 9009'}
              className="border border-orange text-orange h-headerBtn px-4 rounded text-13 flex items-center"
            >
              <i className="pr-1">{Call(orange)}</i>+65 6886 9009
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header