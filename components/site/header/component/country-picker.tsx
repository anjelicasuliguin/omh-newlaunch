import React from 'react'
import Link from 'next/link'

// Context
import { AppContext } from '../../../../pages/_app'

// Flags
import SGP from '../../../../assets/svg/flags/sg'
import MYS from '../../../../assets/svg/flags/my'

// active country
const country = {
  'SGP': SGP(),
  'MYS': MYS()
}

function countryPicker() {
  const Global = React.useContext(AppContext)
  const pageCountry = Global['state']['pageCountry']
  const setPageCountry = Global['state']['setPageCountry']
  return (
    <div>
      <div data-header="user-block">
        <div className="flex items-center cursor-pointer text-sm text-blue border border-white border-b-0 group-hover:border-grey-light rounded-t-lg"
        >
          <span
            className="text-15 text-darkGray"
          >
            {country[pageCountry]}
          </span>
        </div>

        <div data-header="user-block-options" className="shadow overflow-hidden absolute w-56 hidden">
          <Link
            href="/sg"
          >
            <a
              className="no-underline bg-white hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray"
              onClick={() => setPageCountry('SGP')}
            >
              <span>{SGP()}</span>
            </a>
          </Link>
          <Link
            href="/my"
          >
            <a
              className="no-underline bg-white hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray"
              onClick={() => setPageCountry('MYS')}
            >
              <span>{MYS()}</span>
            </a>
          </Link>
        </div>

      </div>
      <style jsx>{`
        [data-header="user-block"]:hover > [data-header="user-block-options"] {
          display: block !important;
        }
        span {
          display: block;
          width: 29px;
          height: 18px;
        }
      `}</style>
    </div>
  )
}

export default countryPicker
