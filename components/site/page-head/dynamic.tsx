import React from 'react'
import NextHead from 'next/head'

const defaultTitle = 'Ohmyhome | Home'
const defaultDescription = ''
const defaultOGURL = ''
const defaultOGImage = ''

function Head({
  title = defaultTitle,
  description = defaultDescription,
  url = '',
  ogImage = '',
  ogTitle = '',
  ogDesc = '',
  ogTags = ''
}) {
  return (
    <NextHead>
      <title>{title || defaultTitle}</title>
      <meta
        name="description"
        content={description || defaultDescription}
      />
      <meta name="keywords" content={ogTags || ''} />
      <meta property="og:url" content={url || defaultOGURL} />
      <meta property="og:title" content={ogTitle || defaultTitle} />
      <meta
        property="og:description"
        content={ogDesc || defaultDescription}
      />
      <meta name="twitter:site" content={url || defaultOGURL} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content={ogImage || defaultOGImage} />
      <meta property="og:image" content={ogImage || defaultOGImage} />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
    </NextHead>
  )
}

export default Head
