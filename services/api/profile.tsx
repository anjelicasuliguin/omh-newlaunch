import request from './request'
import API from './endpoints'

function updateProfile(bearer: string, country: string, data: Object) {
  return request({
    url: API.UPDATE_INFO,
    method: "POST",
    headers: {
      "Authorization": `Bearer ${bearer}`,
      "Accept-Country": country
    },
    data: data
  })
}

function updateProfileImage(bearer: string, country: string, key: string) {
  return request({
    url: API.PROCESS_IMAGE,
    method: "POST",
    headers: {
      "Authorization": `Bearer ${bearer}`,
      "Accept-Country": country,
      "Content-Type": 'application/json',
    },
    data: {
      "key": key
    }
  })
}

function fetchPresign(bearer: string, country: string) {
  return request({
    url: API.PRESIGN_URL,
    method: "GET",
    headers: {
      "Authorization": `Bearer ${bearer}`,
      "Accept-Country": country
    },
  })
}

function uploadProfileToUrl(url: string, bearer: string, country: string, data) {
  return request({
    url: url,
    method: "PUT",
    headers: {
      "Authorization": `Bearer ${bearer}`,
      "Accept-Country": country,
      "Content-Type": 'application/x-www-form-urlencoded',
    },
    body: data
  })
}

const ProfileAPI = {
  updateProfile,
  updateProfileImage,
  fetchPresign,
  uploadProfileToUrl,
}

export default ProfileAPI