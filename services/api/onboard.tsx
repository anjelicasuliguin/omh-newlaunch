
import request from './request'
import API from './endpoints'

function updateMobile(params, bearer) {
  return request({
    url: API.UPDATE_MOBILE,
    method: "POST",
    data: {
      countryCode: params.countryCode,
      mobile: params.mobile,
    },
    headers: {
      Authorization: bearer
    }
  })
}

function updateValidateMobile(pin, bearer) {
  return request({
    url: API.UPDATE_VALIDATE_MOBILE,
    method: "POST",
    data: {
      pin
    },
    headers: {
      Authorization: bearer
    }
  })
}

/*
** Sign up
*/
function signup(values) {
  return request({
    url: API.SIGNUP,
    method: "POST",
    headers: {
      Authorization: 'public'
    },
    data: {
      ...values
    }
  })
}

function verifyMobile(pin, token) {
  return request({
    url: API.VERIFY_MOBILE,
    method: "POST",
    headers: {
      Authorization: 'public'
    },
    data: {
      pin,
      token
    }
  })
}

function verifyConfirm(token) {
  return request({
    url: API.VERIFY_CONFIRM,
    method: "POST",
    headers: {
      Authorization: 'public'
    },
    data: {
      token
    }
  })
}

function addUserRole(userData, bearer) {
  return request({
    url: API.ADD_USER_ROLE,
    method: "POST",
    data: userData,
    headers: {
      Authorization: bearer
    }
  })
}

function signupFacebook(data) {
  return request({
    url: API.FACEBOOK,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

function signupGoogle(data) {
  return request({
    url: API.GOOGLE,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

/*
** Login
*/
function loginMobile(data) {
  return request({
    url: API.LOGIN_MOBILE,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

function forgotPassword(data) {
  return request({
    url: API.LOGIN_FORGOT_PASSWORD,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

function loginFacebook(data) {
  return request({
    url: API.LOGIN_FACEBOOK,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

function loginGoogle(data) {
  return request({
    url: API.LOGIN_GOOGLE,
    method: "POST",
    data: data,
    headers: {
      Authorization: "public"
    }
  })
}

const OnboardService = {
  addUserRole,
  signup,
  verifyMobile,
  verifyConfirm,
  loginMobile,
  forgotPassword,
  signupFacebook,
  signupGoogle,
  loginFacebook,
  loginGoogle,
  updateMobile,
  updateValidateMobile,
}

export default OnboardService