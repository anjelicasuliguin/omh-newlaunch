import React from 'react'
import axios from 'axios'
import PageLayout from '../../../../layouts'
import styled from 'styled-components'
import MainSection from '~/www/components/MainSection'
import BannerSlider from '~/www/components/Banner/group'
import DistrictGroup from '~/www/components/District/group'
import UspGroup from '~/www/components/USP/group'
import CTA from '~/www/components/CTA'
import NewlaunchGroup from '~/www/components/Newlaunch/group'
import NewlaunchCard from '~/www/components/Newlaunch/'
import BlogGroup from '~/www/components/Blog/group'
import '~/styles/tailwind.css'

import NewLaunchesList from '~/www/services/newlaunches'

import {
  bannerConfig,
  districtConfig,
  uspConfig,
  blogConfig,
  newlaunchConfig
} from '~/www/modules'



const OmhSg = () => {
  const [ formConfig, setFormConfig ] = React.useState({})
  const [ newlaunchConfigApi, setNewLaunchesList ] = React.useState({
    slogan: '',
    description: '',
    featured_image: '',
  })

  React.useEffect(() => {
    async function fetchDetails() {
  
      NewLaunchesList
      .getNewLaunchesList()
      .then(response => {
        const { data } = response
        
        data.data && data.data && setNewLaunchesList(data.data)
        console.log(setNewLaunchesList(data.data))
      })
    } fetchDetails()
  
  }, [])
  
  const {
    slogan,
    description,
    featured_image,
  } = newlaunchConfigApi
return (

    <div className={'mt-70'}>
      <div className={'mt-80'}>
        <BannerSlider {...bannerConfig}/>
          <MainSection 
            marginMobile = {'0 0 45px'}
            marginDesktop = {'0 0 68px'}
            title = {'New launches'}
            subtitle = {'Discover Singapore’s newly-launched properties'}
            url = {'#'}
            button = {'See more projects'}>
              <NewlaunchGroup {...newlaunchConfig} />
              {/* <NewlaunchGroup {...newlaunchConfigApi} />
              <NewlaunchCard 
								featured_image = {featured_image}
								slogan = {slogan}
								description = {description}
							/> */}
          </MainSection>
          <MainSection
            marginMobile = {'0 0 45px'}
            marginDesktop = {'0 0 89px'}
            title = {'Explore by districts'}
            subtitle = {'Discover Singapore’s newly-launched properties'}>
              <DistrictGroup {...districtConfig} />
          </MainSection>
          <MainSection 
            marginMobile = {'0 0 45px'}
            marginDesktop = {'0 0 79px'}
            titlePosition = {'center'}
            title = {'What’s included'}
            subtitle = {'Discover Singapore’s newly-launched properties'}>
            <UspGroup {...uspConfig} />
          </MainSection>
          <MainSection 
            background = {'#F7F6F7'}
            marginMobile = {'0 0 45px'}
            marginDesktop = {'0 0 73px'}>
            <CTA 
              image = {'https://api.omh.app/store/cms/media/singapore/newlaunch/open-doodles-reading-side-1.svg'}
              title = {'Get updates on latest properties in Singapore'}
              excerpt = {'Sign up and get daily updates'}
              url = {'#'}
              button = {'Sign up'}
            />    
          </MainSection> 
          <MainSection 
            marginMobile = {'0 0 56px'}
            marginDesktop = {'0 0 95px'}
            title = {'Helpful reads'}
            subtitle = {'Blogs that will help you on your home buying journey'}
            url = {'#'}
            button = {'See more projects'}
          >
            <BlogGroup {...blogConfig} />
          </MainSection>
      </div>
      <style jsx>{`

      `}</style>
    </div>
)}

export default PageLayout(OmhSg, 'default')
