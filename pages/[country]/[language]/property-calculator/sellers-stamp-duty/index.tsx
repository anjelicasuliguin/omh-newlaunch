import React from 'react'
import MainCalculator from 'omh-calculators'
import { useRouter } from 'next/router'
import * as TagManager from 'react-gtm-module'
import PageLayout from '../../../../../layouts'
import Head from '../../../../../components/site/page-head/dynamic'

import '../../../../../styles/tailwind.css'
import { AppContext } from ".././../../../_app"

const COUNTRY = {
  'sg': 'SGP',
  'my': 'MYS',
}

const SellersStampDuty = () => {
  const value = React.useContext(AppContext)
  const val = value['state']
  // const countryList = val['countryList']
  const env = val['environment']
  const router = useRouter()
  const { language = 'en', country = 'sg' } = router && router.query || {}
  const lang = language && language.toString() 
  const cty = country && country.toString() 
  let url = 'https://ohmyhome.com/sg/en/property-calculator/sellers-stamp-duty'
  if (typeof window !== 'undefined' && router.pathname === "/[country]/[language]/property-calculator/sellers-stamp-duty") {
    url = window.location.href
    const tagManagerArgs = {
      dataLayer: {
        countryCode: `${COUNTRY[cty]}`,
        environmentCode: `${COUNTRY[cty]}-${env}`,
        pageName: 'seller_stamp_duty'
      }
    }
    TagManager.dataLayer(tagManagerArgs)
  }

  return (
    <React.Fragment>
      <Head
        title="Ohmyhome | Seller's Stamp Duty Calculator"
        description={"To calculate seller's stamp duty"}
        url={url}
        ogTitle={"Ohmyhome | Financial Calculators | Property Taxes | Plan your finances when Selling your Property | Seller's Stamp Duty Calculator | Financial Tools for Home Sellers"}
        ogDesc={"What are the costs and fees that you need to take into account when you sell your property. The shorter your holding period is to your residential property, the higher the Seller's Stamp Duty (SSD) that you need to pay. Calculate your property taxes with SSD calculator powered by IRAS."}
        ogTags={"Ohmyhome Financial Calculators, financial planning tools, sell property, stamp duty calculators, property taxes, seller's stamp duty, SSD, selling HDB"}
        ogImage={"https://z.omh.sg/media/singapore/Calculator_Meta_image.png"}
      />
      <div className={'main__content'}>
        <MainCalculator
          calcuType={'seller-stamp-duty'}
          lang={lang && lang.toString().toUpperCase() || 'EN'}
        />
      </div>
      <style jsx>{`
        .main__content{
          margin-top: 56px;
        }
        @media (min-width : 1224px){
          .main__content{
            margin-top: 70px;
          }
        }
      `}
      </style>
    </React.Fragment>
  )
}

export default PageLayout(SellersStampDuty, 'standalone')
