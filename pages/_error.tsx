import React from 'react'
import { useRouter } from 'next/router'

const routes = [
  "/[country]/[language]/property-calculator/affordability-calculator",
  "/[country]/[language]/property-calculator/buyers-stamp-duty",
  "/[country]/[language]/property-calculator/sellers-stamp-duty",
  "/[country]/[language]/new-property-launch/[slug]",
  "/[country]/[language]/home",
  "/[country]/[language]/prelaunch",
  "/[country]/[language]/newlaunches",
]

function Error({ statusCode }) {
  const router = useRouter()
  React.useEffect(() => {
    if(router && router.pathname && routes.indexOf(router.pathname) === -1)
      location.href = 'https://omh.sg/'
  })

  return (
    <p>
      {statusCode
        ? `An error ${statusCode} occurred on server`
        : 'You will redirected to https://omh.sg'}
    </p>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default Error