import React from 'react';
import axios from 'axios'
import Cookies from 'js-cookie'
import * as TagManager from 'react-gtm-module'
import { useRouter } from 'next/router'
// context
export const AppContext = React.createContext({})

// components
import Head from '../components/site/page-head'
// import Login from '../components/Onboard/desktop'

// services
import { refreshToken } from '../services/api/auth'

const GTM_CONFIG = {
  gtmId: 'GTM-TCPHJ4X',
  auth: 'yYthFGS5OlfDoYHliv4hiw',
  preview: 'env-2'
}

if(typeof window !== 'undefined') {
  TagManager.initialize(GTM_CONFIG)
}

const GET_ENV = process.env.NODE_ENV !== 'production' ? 'QA' : 'PROD'

function OMHWeb(props) {
  const [drawerStatus, setDrawerStatus] = React.useState(false)
  const [bearer, setBearer] = React.useState()
  const [userData, setUserData] = React.useState({})
  const [pageCountry, setPageCountry] = React.useState("SGP")
  const [loginState, setLoginState] = React.useState(false)
  const [sideMenuState, setSideMenuState] = React.useState(false)
  const [countryList, setCountryList] = React.useState([])
  const [redirect, setRedirect] = React.useState(false)
  const [globalConfig, setGlobalConfig] = React.useState(false)
  const [environment] = React.useState(GET_ENV)

  const { Component, pageProps } = props

  const hooks = {
    drawerStatus,
    setDrawerStatus,
    bearer,
    setBearer,
    userData,
    setUserData,
    pageCountry,
    setPageCountry,
    loginState,
    setLoginState,
    sideMenuState,
    setSideMenuState,
    countryList,
    environment,
    globalConfig,
    setGlobalConfig,
  }

  const router = useRouter()
  const { country = 'sg', language = 'en' } = router && router.query || {}
  Cookies.set('omh-lang', language)
  Cookies.set('omh-country', country)

  const routes = [
    "/[country]/[language]/property-calculator/affordability-calculator",
    "/[country]/[language]/property-calculator/buyers-stamp-duty",
    "/[country]/[language]/property-calculator/sellers-stamp-duty",
    "/[country]/[language]/new-property-launch/[slug]",
    "/[country]/[language]/home",
    "/[country]/[language]/prelaunch",
    "/[country]/[language]/newlaunches",
  ]

  React.useEffect(() => {
    setRedirect(false)

    if(router && router.pathname && routes.indexOf(router.pathname) === -1)
      setRedirect(true)
    /*
    if('serviceWorker' in navigator) {
      window.addEventListener('load', function () {
        navigator.serviceWorker.register('/service-worker.js').then(function (registration) {
          console.log('SW registered: ', registration)
        }).catch(function (registrationError) {
          console.log('SW registration failed: ', registrationError)
        })
      })
    }
    */

    /*
    ** Refresh auth token
    */
    Cookies.get('omh_cookie') !== undefined
      && (
        refreshToken(Cookies.get('omh_cookie'))
          .then(response => {
            setUserData(response.data._data.user)
          })
      )

    async function fetchData() {
      const response = await axios('https://qa.ohmyhome.com.my/api/global/config')
      setGlobalConfig(response.data)
      setCountryList(response.data.countries)
    }

    fetchData()
  }, [])

  return (
    <AppContext.Provider
      value={{
        state: hooks
      }}
    >
      {
        !redirect &&
        <React.Fragment>
          <Head
            title="OMH"
          />
          <Component {...pageProps} />
          {/* <Login
            loginState={loginState}
            setLoginState={setLoginState}
          /> */}
        </React.Fragment>
      }
    </AppContext.Provider>
  )
}

export default OMHWeb
