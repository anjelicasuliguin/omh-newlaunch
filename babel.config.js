module.exports = function (api) {
  api.cache(true);
  const presets = [
    ['next/babel',  { 'transform-runtime': { useESModules: false } }],
    '@zeit/next-typescript/babel',
    '@babel/preset-typescript'
  ]
  const plugins = [
    ["babel-plugin-tailwind-components", { "format": "string" }],
    "css-modules-transform",
    [
      "babel-plugin-root-import",
      {
        "rootPathSuffix": "./",
        "rootPathPrefix": "~/"
      }
    ]
  ]
  return {
    presets,
    plugins
  };
}
