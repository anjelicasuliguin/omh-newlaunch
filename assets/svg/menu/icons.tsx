

import React from "react"

const House = (
  <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path opacity="0.7" fill-rule="evenodd" clip-rule="evenodd" d="M8.5 0L8.1237 0.24349L0.332031 5.06901L0 5.26823V7.79167H0.708333V17H16.2917V7.79167H17V5.26823L16.668 5.06901L8.8763 0.24349L8.5 0ZM8.5 1.68229L15.5833 6.0651V6.375H14.875V15.5833H2.125V6.375H1.41667V6.0651L8.5 1.68229ZM7.79167 6.375V5.66667H9.20833V6.375C10.3732 6.375 11.3333 7.33512 11.3333 8.5H9.91667C9.91667 8.0988 9.60954 7.79167 9.20833 7.79167H7.79167C7.39046 7.79167 7.08333 8.0988 7.08333 8.5C7.08333 8.9012 7.39046 9.20833 7.79167 9.20833H9.20833C10.3732 9.20833 11.3333 10.1685 11.3333 11.3333C11.3333 12.4982 10.3732 13.4583 9.20833 13.4583V14.1667H7.79167V13.4583C6.62679 13.4583 5.66667 12.4982 5.66667 11.3333H7.08333C7.08333 11.7345 7.39046 12.0417 7.79167 12.0417H9.20833C9.60954 12.0417 9.91667 11.7345 9.91667 11.3333C9.91667 10.9321 9.60954 10.625 9.20833 10.625H7.79167C6.62679 10.625 5.66667 9.66488 5.66667 8.5C5.66667 7.33512 6.62679 6.375 7.79167 6.375Z"
        fill="#343434"
      />
    </svg>
)

export default function Icons(icon) {
  let Icon: any

  switch(icon) {
    case 'find_a_home':
      Icon = House
      break
    case 'post_a_property':
      Icon = House
      break
    case 'get_an_agent':
      Icon = House
      break
    case 'services':
      Icon = House
      break
    default:
      Icon = House
  }

  return (
    <React.Fragment>
      {Icon}
    </React.Fragment>
  )
}
