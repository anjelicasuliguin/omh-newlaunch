import React from "react"

export default function FlagMy() {
  return (
    <span>
      <svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_d)">
          <rect x="4" y="2" width="22" height="12.8864" fill="#D50000" />
          <rect x="4" y="14.4092" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="12.5" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="10.5908" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="8.68188" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="6.77271" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="4.86353" width="22" height="0.954545" fill="#ECEFF1" />
          <rect x="4" y="3.02271" width="22" height="0.886295" fill="#ECEFF1" />
          <rect x="4" y="2" width="13" height="7.63636" fill="#311B92" />
          <path fillRule="evenodd" clipRule="evenodd" d="M9.01 2.98853C7.3485 2.98853 6 4.27143 6 5.85216C6 7.43289 7.3485 8.7158 9.01 8.7158C9.6105 8.7158 10.1695 8.54828 10.6395 8.25953C10.366 8.34973 10.0725 8.39889 9.767 8.39889C8.2935 8.39937 7 7.22003 7 5.81828C7 4.41653 8.2935 3.32071 9.7665 3.32071C10.092 3.32071 10.404 3.37607 10.6925 3.47773C10.212 3.16941 9.633 2.98853 9.01 2.98853Z" fill="#FFD600" />
          <path fillRule="evenodd" clipRule="evenodd" d="M14.5015 7.30877L13.234 6.67734L13.609 7.96979L12.761 6.88877L12.496 8.20461L12.236 6.88829L11.3845 7.96693L11.7635 6.67591L10.4935 7.30352L11.437 6.29313L10 6.34563L11.321 5.81634L10.002 5.28323L11.4385 5.34002L10.4985 4.32725L11.7665 4.95868L11.3915 3.66623L12.2395 4.74725L12.504 3.43188L12.764 4.7482L13.6155 3.66957L13.2365 4.96059L14.5065 4.33298L13.563 5.34336L15 5.29086L13.679 5.82016L14.998 6.35327L13.5615 6.29648L14.5015 7.30877Z" fill="#FFD600" />
        </g>
        <defs>
          <filter id="filter0_d" x="0" y="0" width="30" height="21.3637" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
            <feFlood floodOpacity="0" result="BackgroundImageFix" />
            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
            <feOffset dy="2" />
            <feGaussianBlur stdDeviation="2" />
            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0832878 0" />
            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
          </filter>
        </defs>
      </svg>
      <style jsx>{`
        span {
          box-shadow: 0px 3px 10px -5px rgba(0,0,0,1);
        }
      `}</style>
    </span>
  )
}
